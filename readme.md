## Mudraa Android and iOS App

## How to use?

*This app does not work on its own*. It is missing the Ionic library, and AngularJS.

To use this, either create a new ionic project using the ionic node.js utility, or copy and paste this into an existing Cordova project and download a release of Ionic separately.

```bash
$ sudo npm install -g ionic cordova
$ ionic start myApp sidemenu
```

Then, to run it, cd into `myApp` and run:

```bash
$ ionic platform add ios
$ ionic build ios
$ ionic emulate ios
```

## File and Folder Structure

You will find a lot of files and folders under this repository but the main ones are as follows...

```platform```: Folder where the build of the app is generated like .apk for android and a folder for ios. You can generate the build by running...

```bash
$ ionic cordova platfrom add android
```

```resources```: Stores the app icons and splash screens. For more info visit [https://ionicframework.com/docs/cli/cordova/resources/](https://ionicframework.com/docs/cli/cordova/resources/)


```www/js/app.js```: Place where we define all the routes and its views. This is also the first file to initiate when the app is launched.

```www/js/controller/mudraa/Controller.js```: All the rotues and view mapped in app.js is mapped here. Every function has a role in this file. 


```www/js/template/mudraa```: Every route has to be mapped to a view and all the frontend view are stored here. 