// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic','ionic.service.core', 'starter.controllers',
  'ngCordova',
  'ngCordovaOauth',
  'chart.js',
  'ion-floating-menu',
  'pascalprecht.translate',
  'ionic-modal-select',
  'ionic-toast',
  ])

.run(function($ionicPlatform, $ionicPopup, $rootScope, $location, $state, $http) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        var apiToken = window.localStorage['loginid'];
        if(!apiToken) {
          $location.path("/login");
        }
    });

    $ionicPlatform.ready(function() {
        if(window.navigator.splashscreen) {
          navigator.splashscreen.hide();
        }

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }

        if(window.Connection) {
            if(navigator.connection.type == Connection.NONE) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Network Not Found',
                    template: '<center>We could not establish a connection to our server, please verify your internet settings and try again.</center>'
                });

                alertPopup.then(function(res) {
                    ionic.Platform.exitApp();
                });
            }
        }

        if (window.StatusBar) {
          // org.apache.cordova.statusbar required
          if(device.platform == 'iOS') {
            StatusBar.overlaysWebView(true);
          } else {
            StatusBar.overlaysWebView(false);
          }
          StatusBar.backgroundColorByHexString("#2f4050");
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $ionicAppProvider, $ionicConfigProvider, $translateProvider) {

  $ionicConfigProvider.tabs.position('bottom');
  $stateProvider
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/v2/tabs.html",
      controller: 'AppCtrl'
    })

    .state('tab.home', {
      url: '/home',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/mudraa/home.html',
          controller: 'AppCtrl'
        }
      }
    })

    .state('tab.posts', {
      url: '/posts',
      cache: false,
      views: {
        'tab-posts': {
          templateUrl: 'templates/mudraa/posts.html',
          controller: 'PostsCtrl'
        }
      }
    })

    .state('tab.topics', {
      url: '/topics',
      views: {
        'tab-topics': {
          templateUrl: 'templates/mudraa/topics.html',
          controller: 'TopicsCtrl'
        }
      }
    })

    .state('post-details', {
      url: '/post-details/:messid',
      cache: false,
      templateUrl: 'templates/mudraa/post-details.html',
      controller: 'PostDetailCtrl'
    })

    .state('tab.scanner', {
      url: '/scanner',
      cache: false,
      views: {
        'tab-scanner': {
          templateUrl: 'templates/mudraa/scanner.html',
          controller: 'AppCtrl'
        }
      }
    })

    .state('tab.profile', {
      url: '/profile',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/mudraa/profile.html',
          controller: 'LoginRegisterCtrl'
        }
      }
    })

    .state('posts', {
      url: '/posts/:stockcode',
      cache: false,
      templateUrl: 'templates/mudraa/posts.html',
      controller: 'PostsCtrl'
    })

    .state('user-posts', {
      url: '/user-posts/:loginId',
      cache: false,
      templateUrl: 'templates/mudraa/user-posts.html',
      controller: 'PostsCtrl'
    })

    .state('user-inbox', {
      url: '/user-inbox',
      cache: false,
      templateUrl: 'templates/mudraa/user-inbox.html',
      controller: 'LoginRegisterCtrl'
    })

    .state('user-bookmark', {
      url: '/user-bookmark',
      cache: false,
      templateUrl: 'templates/mudraa/user-bookmark.html',
      controller: 'LoginRegisterCtrl'
    })

    .state('follow', {
      url: '/follow/:followType/:loginId',
      cache: false,
      templateUrl: 'templates/mudraa/follow.html',
      controller: 'LoginRegisterCtrl'
    })

    .state('timeline', {
      url: '/timeline',
      cache: false,
      templateUrl: 'templates/mudraa/timeline.html',
      controller: 'LoginRegisterCtrl'
    })

    .state('register', {
      url: '/register',
      cache: false,
      templateUrl: 'templates/mudraa/register.html',
      controller: 'LoginRegisterCtrl'
    })

    .state('login', {
        url: '/login',
        templateUrl: 'templates/mudraa/login.html',
        controller: 'LoginRegisterCtrl'
    });

    // if none of the above states are matched, use this as the fallback
    var apiToken = window.localStorage['apiToken'];
    var eventCode = window.localStorage['eventCode'];

    $urlRouterProvider.otherwise('/tab/home');
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = false;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
});
