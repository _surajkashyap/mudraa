angular.module('starter.controllers', ['ionic', 'ngCordova'])
.controller('AppCtrl', function($scope, $translate, $rootScope,$ionicHistory, $ionicPopup, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location) {

    $scope.goBack = function() {
       window.history.go(-1);
    }

    $scope.initOnlineUsers = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/online-user-json.php',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.onlineUsers = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initPosts = function($limit) {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/topic-thread-list-json-pagination.php?limit='+$limit,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.posts = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initPosts(3);
    $scope.initOnlineUsers();

})

.controller('TopicsCtrl', function($scope, $translate, $rootScope,$ionicHistory, $ionicPopup, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location) {

    $scope.query = '';

    $scope.goBack = function() {
       window.history.go(-1);
    }

    $scope.initTopics = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/topic-list-json.php',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.topics = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.clearSearch = function() {
        $scope.query = '';
    }

    $scope.initTopics();
})

.controller('PostsCtrl', function($scope, $translate, $rootScope, $stateParams, $ionicHistory, $ionicPopup, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location) {

    $scope.stockcode = $stateParams.stockcode;
    $scope.loginId = $stateParams.loginId;

    $scope.goBack = function() {
       window.history.go(-1);
    }

    $scope.initPosts = function() {
        var url = baseUrl + '/android/topic-thread-list-json-pagination.php?stockcode='+$scope.stockcode
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.posts = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initUserPosts = function() {
        var url = baseUrl + '/android/user-posts-json.php?loginid='+$scope.loginId
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userPosts = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

     $scope.follow = function(userId) {
        var req = {
            method: 'GET',
            url: baseUrl + '/android/followuser-json.php?userid='+window.localStorage['loginid']+'&otheruserid='+userId+'&access_token='+window.localStorage['access_token'],
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.isFollowing();
            ionicToast.show(data.response, 'bottom', false, 3000);
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

     $scope.unfollow = function(userId) {
        var req = {
            method: 'GET',
            url: baseUrl + '/android/unfollowuser-json.php?userid='+window.localStorage['loginid']+'&otheruserid='+userId+'&access_token='+window.localStorage['access_token'],
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.isFollowing();
            ionicToast.show(data.response, 'bottom', false, 3000);
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.isFollowing = function() {
        var req = {
            method: 'GET',
            url: baseUrl + '/android/follow-check-json.php?userid='+window.localStorage['loginid']+'&otheruserid='+$scope.loginId,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            window.localStorage['isUserFollowing'] = data.response;
            $scope.isUserFollowing = window.localStorage['isUserFollowing'];
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initUserPosts();
    $scope.initPosts();
    $scope.isFollowing();

})

.controller('PostDetailCtrl', function($scope, $translate, $rootScope, $stateParams, $ionicHistory, $ionicScrollDelegate, $ionicPopup, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location) {

    $scope.messid = $stateParams.messid;
    $scope.scrollToTopIsEnabled = false;

    $scope.goBack = function() {
       window.history.go(-1);
    }

    $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
    $scope.initPostDetail = function() {
        var req = {
            method: 'GET',
            url: baseUrl + '/android/singlepost-json1.php?messid='+$scope.messid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.postDetail = data[0];
            $('#postDescription').html($scope.postDetail.description);
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.bookmark = function(messid) {
       var req = {
            method: 'GET',
            url: baseUrl + '/android/bookmarkthread-json.php?userid='+window.localStorage['loginid']+'&messid='+messid+'&access_token='+window.localStorage['access_token'],
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            ionicToast.show(data.response, 'bottom', false, 3000);
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.like = function(messid) {
       var req = {
            method: 'GET',
            url: baseUrl + '/android/likethread-json.php?userid='+window.localStorage['loginid']+'&messid='+messid+'&access_token='+window.localStorage['access_token'],
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            ionicToast.show(data.response, 'bottom', false, 3000);
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initMessageReplies = function($limit) {

        var req = {
            method: 'GET',
            url: baseUrl + '/android/singlepost-json2.php?messid='+$scope.messid+'&limit='+$limit,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.postReplies = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.scrollToTop = function() {
        $ionicScrollDelegate.scrollTop();
    }

    $scope.getScrollPosition = function(){
        if ($ionicScrollDelegate.getScrollPosition().top > 800) {
            $scope.scrollToTopIsEnabled = true;
            $('#scrollToTop').show();
        } else {
            if ($ionicScrollDelegate.getScrollPosition().top > 800) {
               $scope.scrollToTopIsEnabled = true;
               $('#scrollToTop').show();
            } else {
                $('#scrollToTop').hide();
               $scope.scrollToTopIsEnabled = false;
            }
        }
    }


    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    $scope.initMessageReplies('');
    $scope.initPostDetail();
})

.controller('ScanCtrl', function($scope, $state, $ionicHistory, $stateParams, $rootScope, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location, $ionicScrollDelegate) {
})
.controller('LoginRegisterCtrl', function($scope, $stateParams, $state, $translate, $rootScope, $ionicModal, $timeout, $cordovaOauth, $ionicLoading, $http, $ionicActionSheet, $ionicPopup, ionicToast, $rootScope, $ionicUser, $cordovaLocalNotification, $location) {

    $scope.loginData = {};
    $scope.registerData = {};
    $scope.followType = $stateParams.followType;
    $scope.loginId = $stateParams.loginId;

    $scope.goBack = function() {
       window.history.go(-1);
    }

    $scope.logout = function(){
        $ionicLoading.show({template: 'Logging out...'});
        localStorage.removeItem('');
        localStorage.removeItem('loginid');
        ionicToast.show('You have been logged out.', 'bottom', false, 3000);
        location.reload();
    }

    $scope.registerUser = function() {
        if(!$scope.registerData.loginid ||
                !$scope.registerData.password ||
                !$scope.registerData.firstname ||
                !$scope.registerData.lastname ||
                !$scope.registerData.city ||
                !$scope.registerData.mobile ||
                !$scope.registerData.emailid
            ) {
            ionicToast.show('All fields are mandatory.', 'bottom', false, 3000);
            return false;
        }

        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });
        var req = {
            method: 'POST',
            url: baseUrl + '/android/register-app.php',
            data: $.param($scope.registerData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.response == 'Success') {
                window.localStorage['loginid'] = $scope.registerData.loginid;
                $ionicLoading.hide();
                $state.go('tab.home');
                setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                $ionicLoading.hide();
                ionicToast.show('Please verify your details once again.', 'bottom', false, 3000);
            }
        }).error(function(data) {
            $ionicLoading.hide();
            ionicToast.show(data.message, 'bottom', false, 3000);

        });
    }

    $scope.loginUser = function() {
        if(!$scope.loginData.username ||
                !$scope.loginData.password
            ) {
            ionicToast.show('All fields are mandatory.', 'bottom', false, 3000);
            return false;
        }

        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });
        var req = {
            method: 'POST',
            url: baseUrl + '/android/oauth2/authorize.php?state=xyz&redirect_uri=https%3A%2F%2Fwww.mudraa.com&response_type=code&client_id=androidapp&scope=basic&access_type=offline',
            data: $.param($scope.loginData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        $http(req).success(function(data) {
            if (data.code) {
                var accessTokenData = $scope.getAccessToken(data.code);
                if(window.localStorage['access_token']) {
                    window.localStorage['loginid'] = $scope.loginData.username;
                    $ionicLoading.hide();
                    $state.go('tab.home');
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                } else {
                    $ionicLoading.hide();
                    ionicToast.show('Something went wrong while retriving access token.', 'bottom', false, 3000);
                }
            } else {
                $ionicLoading.hide();
                ionicToast.show('Please verify your details once again.', 'bottom', false, 3000);
            }

        }).error(function(data) {
            $ionicLoading.hide();
            ionicToast.show('Something went wrong', 'bottom', false, 3000);

        });
    }

    $scope.getAccessToken = function(authCode){

        var tokenData = {code: authCode, authorization_code:authCode, grant_type: "authorization_code", client_id: "androidapp", client_secret:"androidmudraa", redirect_uri: 'https://www.mudraa.com'};
        $ionicLoading.show({
            template: '<ion-spinner icon="lines" class="light"></ion-spinner>'
        });

        var req = {
            method: 'POST',
            url: baseUrl + '/android/oauth2/token.php',
            data: $.param(tokenData),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            if (data.access_token) {
                window.localStorage['access_token'] = data.access_token;
                window.localStorage['refresh_token'] = data.refresh_token;
                return true;
            } else {
                return false;
            }

        }).error(function(data) {
            return false;
        });
    }

    $scope.initFollow = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/user-'+$scope.followType+'-json.php?loginid='+$scope.loginId,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.follows = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initUserInbox = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/user-inbox-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userInbox = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initUserBoomark = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/bookmarked-threads-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userBookmarks = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.oauth = function() {
        window.plugins.googleplus.login(
            {
              'scopes': 'basic' // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
            },
            function (obj) {
              alert(JSON.stringify(obj)); // do something useful instead of alerting
            },
            function (msg) {
              alert('error: ' + msg);
            }
        );
    }

    $scope.initTimeline = function() {
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});
        var req = {
            method: 'GET',
            url: baseUrl + '/android/my-timeline-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.timelines = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });
    }

    $scope.initUserProfile = function() {
        $scope.loginid = window.localStorage['loginid'];
        //$scope.loginid = 'leezi';
        $ionicLoading.show({template: '<ion-spinner icon="lines" class="light"></ion-spinner>'});

        var req = {
            method: 'GET',
            url: baseUrl + '/android/user-info-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userDetails = data[0];
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function(data) {
            $ionicLoading.hide();
        });

        var req = {
            method: 'GET',
            url: baseUrl + '/android/user-posts-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userPostsCount = data.length;
            $ionicLoading.hide();
        }).error(function(data) {
            $ionicLoading.hide();
        });

         var req = {
            method: 'GET',
            url: baseUrl + '/android/user-followers-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userFollowersCount = data.length;
            $ionicLoading.hide();
        }).error(function(data) {
            $ionicLoading.hide();
        });

        var req = {
            method: 'GET',
            url: baseUrl + '/android/user-following-json.php?loginid='+$scope.loginid,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        $http(req).success(function(data) {
            $scope.userFollowingCount = data.length;
            $ionicLoading.hide();
        }).error(function(data) {
            $ionicLoading.hide();
        });


    }

})
